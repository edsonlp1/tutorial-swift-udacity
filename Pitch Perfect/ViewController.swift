//
//  ViewController.swift
//  Pitch Perfect
//
//  Created by Edson on 23/2/15.
//  Copyright (c) 2015 Edson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textGrabando: UILabel!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnStop: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func viewWillAppear(animated: Bool) {
        //Carga las animaciones que van a ser cargadas.
        btnStop.hidden=true;
        textGrabando.hidden=true;

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func recordAudio(sender: UIButton) {
        // Mostrar texto "Grabación en progreso"
        textGrabando.hidden=false;
        btnStop.hidden=false;
        btnRecord.enabled=false
  
        // Grabar voz del usuario
    
        println("func recordAudio");
    }


    @IBAction func stopAudio(sender: UIButton) {
        //Ocultar texto "Grabación en progreso"
        textGrabando.hidden=true
        btnStop.hidden=true
        btnRecord.enabled=true;
    }
}

